<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    protected $fillable = [
        'cep',
        'cidade',
        'bairro',
        'numero',
    ];

    public function user(){
        return $this->hasMany('App\Funcionario');
    }
}
