@extends('layouts.padrao')

@section('Conteudo')
<div class="container">
<br>
<div class="row"> <!-- 6 + 6 = 12 -->
    <div class="col-md-12">
        <h3>Fila de espera</h3>
        <table class="table table-hover  table-striped text-center">
                <tr>
                    <th>id</th>
                    <th>Modelo</th>
                    <th>Marca</th>
                    <th>Placa</th>
                    <th>Preço</th>
                    <th>Staus</th>
                </tr>
                @foreach ($faf as $carro)
                    <tr>
                    <td>{{$carro->ficha->id}}</td>
                    <td>{{$carro->ficha->modelo}}</td>
                    <td>{{$carro->ficha->marca}}</td>
                    <td>{{$carro->ficha->placa}}</td>
                    <td>{{$carro->ficha->preco}}</td>
                    <td>
                    
                    {{ Form::open(['method'=>'PUT', 'route'=>['fila.update', $carro->id ] ]) }}
                
                        @csrf   
                            {{-- status --}}
                            <div class="form-group">
                                <select class="form-control" name="funcionarios_id" required>
                                    <option value="">ATRIBUIR</option>
                                    @foreach ($funcionario as $fun)
                                        <option value=" {{ $fun->id }} "> {{$fun->nome}}  </option>
                                    @endforeach
                                </select>
                            </div>
                            
                            {{-- funcionario --}}
                            <div class="form-group">
                                <select class="form-control" name="status_id" required>
        
                                    @foreach ($status as $sta)
                                        <option value="{{ $sta->id  }}"> {{ $sta->nome }}  </option>
                                    @endforeach
                                </select>
                            </div>
                            <button class="btn btn-danger" type="submit">Salvar</button>
                        {{ Form::close() }}
                    </td>
                    </tr>
                    @endforeach
            </table>
       
    </div>
    {{-- data-table --}}
    <div class="col-md-12">
      <h3>Finalizados</h3>
            <table class="table table-hover  table-striped text-center">
                    <tr>
                        <th>id</th>
                        <th>Placa</th>
                        <th>Preço</th>
                        <th>Finalizado Por</th>
                       
                    </tr>
                    @foreach ($finalizados as $fin)
                        <tr>
                        <td>{{$fin->id}}</td>
                        <td>{{$fin->ficha->placa}}</td>
                        <td>{{$fin->ficha->preco}}</td>
                        <td>{{$fin->funcionario->nome}}</td>
                     </tr>
                     @endforeach
                </table>
       
    </div>
</div>
    
</div>



@endsection