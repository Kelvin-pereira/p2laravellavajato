<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Nationality;

use App\Http\Controllers\UploadFile;
use App\Http\Controllers\Treatment;
use Session;


class UserController extends Controller
{
    public $validationUserForm = ['name' => 'required|min:3|max:100|string',
         'email' => 'required|min:6|max:150|string'];
      


    public function index()
    {
        $user = User::find(auth()->user()->id);
        return view('user.edit', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('user.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        if (auth()->user()->id != $id) {

            Session::flash('error', 'Operação não permitida!');
            return view('user.edit', compact('user', 'nacionality'));

        }

        $user = User::find(auth()->user()->id);
        $nacionality = Treatment::getToSelect(Nationality::getNationalitys());
        $validation = Treatment::makeValidationForm($request->all(), $this->validationUserForm);

        if (!$validation['status']) {
            Session::flash('error', $validation['message']);
            return view('user.edit', compact('user', 'nacionality'));
        }

        $user = (new User)->find(auth()->user()->id)->fill($request->all());
        if ($request->file()) {

            if ($user->image) {
                UploadFile::deletedFile($user->image);
            }

            $user->image = UploadFile::upload($request->file(), 'IMG');
        }
        $user->save();

        Session::flash('success', 'Dados atualizados com sucesso');
        return view('user.edit', compact('user', 'nacionality'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
