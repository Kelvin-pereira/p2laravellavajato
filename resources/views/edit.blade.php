@extends('layouts.padrao')

@section('Conteudo')
<div class="container">
<br>
<div class="row"> <!-- 6 + 6 = 12 -->
    <div class="col-md-4">
       
    </div>
    <div class="col-md-5">
        <div class="card" style="width: 20rem;">
            <div class="card-body">
                <h5 class="card-title text-center"> Editar dados</h5>
                    {{ Form::open(['method'=>'PUT', 'route'=>['home.update', $ficha->id ] ]) }}
                    @csrf   
                    <div class="input-group">
                        <input type="text" value=" {{ $ficha->marca }} " name="marca" placeholder="Nome marca" class="form-control" required>
                    </div><br>
                    <div class="input-group ">
                        <input type="text" value=" {{ $ficha->modelo }} " name="modelo" placeholder="Nome modelo" class="form-control" required>
                    </div><br>
                    <div class="input-group">
                        <input type="text" value=" {{ $ficha->placa }} " name="placa" placeholder="Nome pacla" class="form-control" required>
                    </div><br>
                    <div class="input-group">
                        <input type="text" value=" {{ $ficha->preco }} " name="preco" placeholder="Preço" class="form-control" required>
                    </div><br>
                    <!-- Submit -->
                    <div class="text-right">
                        <button class="btn btn-danger" type="reset">Limpar</button>
                        <button class="btn btn-info" type="submit">Enviar</button>
                    </div>
                {{ Form::close() }}
            </div>
            </div>
    </div>
   
</div>
    
</div>



@endsection