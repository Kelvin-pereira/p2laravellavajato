<?php

use Illuminate\Database\Seeder;

class Endereco extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('enderecos')->insert([
            [
                'cep'    => "722.36.800",
                'cidade' => "brasilia",
                'bairro' => "SHSN",
                'numero' => "666",
            ],
            
        ]);
       
    }
}
