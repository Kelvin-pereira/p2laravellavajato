<?php

use Illuminate\Database\Seeder;
use App\Statu;

class Status extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status')->insert([
            ['nome' => "Em andamento"],
            ['nome' => "Finalizado"],
        ]);
       
    }
}
