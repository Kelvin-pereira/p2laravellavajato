<?php

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();


Route::group(['middleware' => 'auth'], function () {


    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('/home', 'HomeController');
    Route::resource('/ficha','Ficha\FichaController');
    Route::resource('/funcionario','funcionario\funcionarioController');
    Route::resource('/fila','fila\filaController');

   


});


