<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funcionario extends Model
{
    protected $fillable = [
        'nome',
        'cpf',
        'rg',
        'telefone',
        'enderecos_id',
    ];
    
    public function endereco(){

        return $this->belongsTo('App\Endereco','enderecos_id');

    }
}

