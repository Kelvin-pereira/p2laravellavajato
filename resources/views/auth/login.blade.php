@extends('layouts.login')
@section('content')
    <section class="login">
        <div class="login__form">
            <div class="login__form-icon">
           
            </div>

            {{ Form::open(['url' => 'login','class'=>'form']) }}

            {{ Form::token()  }}

            {{ Form::email('email',null,['class' => 'form-control','placeholder'=>'Email','required']) }}

            {{Form::password('password',['class'=>'form-control','placeholder'=>'Senha','required'])}}

            {{Form::submit('Entrar')}}

            {{Form::close()}}

            <p>
                ou
            </p>
            <br><br>
            <div class="signup-cta">
                <a href="/register" class="btn">Cadastre-se</a>
            </div>
        </div>
    </section>

@endsection





