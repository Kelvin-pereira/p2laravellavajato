<!DOCTYPE html>
<html class="no-js" lang="pt-br">
<head>
    <meta charset="utf-8">
    <title>Speak to Share</title>
    <link href="{{ asset('assets/stylesheets/application.bundle-6f1ad028.css') }}" rel="stylesheet" media="all"/>
    <link href="//cdnjs.cloudflare.com" rel="dns-prefetch">
    <link href="//www.google-analytics.com" rel="dns-prefetch">
    <link href="{{ asset('assets/images/favicon-57e24d0d.png') }}" rel="shortcut icon">
    <meta content="initial-scale=1, user-scalable=yes, width=device-width" name="viewport">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="on" http-equiv="cleartype">
    <meta content="True" name="HandheldFriendly">
    <meta content="320" name="MobileOptimized">
    <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script><![endif]-->
    <script src="{{ asset('assets/javascripts/hours_matrix-568a9f05.js') }} "></script>
</head>
<body>
<noscript>
    <div class="browser-warning">
        <p>
            Your browser has <strong>javascript turned off</strong>. Please <a
                    href="http://www.activatejavascript.org/">enable javascript</a>, or you may need to <a
                    href="http://browsehappy.com/">upgrade your browser</a>.
        </p>
    </div>
</noscript>

@yield('content')

<script src="{{ asset('assets/javascripts/application.bundle-f11a826d.js') }}" defer="defer"></script>
<!--[if lt IE 9]>
<script src="//cdnjs.cloudflare.com/ajax/libs/nwmatcher/1.3.6/nwmatcher.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script><![endif]-->
</body>
</html>