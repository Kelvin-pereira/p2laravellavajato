var selection_json_str=
	'{"selection":[ '+
	'	{"days":[0,1],"hours":[{"from_hour":7,"to_hour":18}]}, '+
	'	{"days":[3],"hours":[{"from_hour":7,"to_hour":8},{"from_hour":16,"to_hour":20}]} '+
	']}';

var matrix = new HoursMatrix(document.getElementById("schedule-calendar"), {hours_vertical:true, hours_view:24, from_hour:9, to_hour:23, hours_step:1}, JSON.parse(selection_json_str));
