@extends('layouts.padrao')

@section('Conteudo')
<div class="container">
<br>
<div class="row"> <!-- 6 + 6 = 12 -->
    <div class="col-md-5">
        <div class="card" style="width: 20rem;">
            <div class="card-body">
                <h5 class="card-title text-center">Automovel</h5>
                <form action="/ficha" method="post" >
                    @csrf   
                    <div class="input-group">
                        <input type="text" name="marca" placeholder="Nome marca" class="form-control" required>
                    </div><br>
                    <div class="input-group ">
                        <input type="text" name="modelo" placeholder="Nome modelo" class="form-control" required>
                    </div><br>
                    <div class="input-group">
                        <input type="text" name="placa" placeholder="Nome pacla" class="form-control" required>
                    </div><br>
                    <div class="input-group">
                        <input type="text" name="preco" placeholder="Preço" class="form-control" required>
                    </div><br>
                    <!-- Submit -->
                    <div class="text-right">
                        <button class="btn btn-danger" type="reset">Limpar</button>
                        <button class="btn btn-info" type="submit">Enviar</button>
                    </div>
                {{ Form::close() }}
            </div>
            </div>
    </div>
    {{-- data-table --}}
    <div class="col-md-7">
      
            <table class="table table-hover  table-striped text-center">
                    <tr>
                        <th>id</th>
                        <th>Modelo</th>
                        <th>Marca</th>
                        <th>Placa</th>
                        <th>Preço</th>
                        <th>Açoes</th>
                    </tr>
                    @foreach ($fila as $carro)
                        <tr>
                        <td>{{$carro->id}}</td>
                        <td>{{$carro->modelo}}</td>
                        <td>{{$carro->marca}}</td>
                        <td>{{$carro->placa}}</td>
                        <td>{{$carro->preco}}</td>
                        <td>
                     
                         {{ Form::open([ 'method'  => 'DELETE', 'route' => [ 'home.destroy', $carro->id ] ]) }}
                         <a href="home/{{ $carro->id }}/edit " class="btn btn-warning ">Editar</a>
                         @csrf   
                         {{-- <button disabled  class="btn btn-danger" type="submit">Excluir</button> --}}
                         <button   class="btn btn-danger" type="submit">Excluir</button>
                         
                         {{ Form::close() }}
             
                     </td>
                     </tr>
                     @endforeach
                </table>
       
    </div>
</div>
    
</div>



@endsection