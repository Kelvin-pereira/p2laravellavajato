<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FichaAtendimentoFuncionario extends Model
{
    protected $fillable = [
        'funcionarios_id',
        'fichas_id',
        'status_id',
    ];

    public function ficha(){

        return $this->belongsTo('App\Ficha','fichas_id');

    }
    public function status(){

        return $this->belongsTo('App\Statu','status_id');

    }
    public function funcionario(){

        return $this->belongsTo('App\Funcionario','funcionarios_id');

    }
}
