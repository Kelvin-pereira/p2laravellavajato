<?php

namespace App\Http\Controllers\Fila;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FichaAtendimentoFuncionario as FAF; 
use App\Funcionario;
use App\Statu;

class FilaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //array object
        $faf = FAF::where('status_id',1)->get();
        $funcionario = Funcionario::all();
        $status = Statu::all();
        $finalizados = FAF::where('status_id',2)->get();

        return view('fila.index',compact('faf','funcionario','status','finalizados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $faf = FAF::find($id);
        $faf->update($request->all());
        $faf->save();

        return redirect(route('fila.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
