<?php

namespace App\Http\Controllers\Funcionario;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Endereco;
use App\Funcionario;
use App\FichaAtendimentoFuncionario as FAF;


class FuncionarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $funcionario = Funcionario::all();
        return view('funcionario.index',compact('funcionario'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $endereco = new Endereco($request->all());
        $funcionario = new Funcionario($request->all());
        $endereco->save();
        $funcionario->enderecos_id = $endereco->id;
        $funcionario->save();
       
        return redirect(route('funcionario.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $func = Funcionario::find($id);
        return view('funcionario.edit',array('func'=>$func));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $func = Funcionario::find($id);
        $func->update($request->all());
        $endereco = Endereco::find($request->enderecoid);
        $endereco->update($request->all());
        $func->save();
        $endereco->save();

        return redirect(route('funcionario.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     
        $faf = FAF::where('funcionarios_id',$id)->get();
        // dd($faf);

        foreach ($faf as $key) {
            $key->delete();
        }
        $func = Funcionario::find($id);
        $func->delete();

        return redirect(route('funcionario.index') );
    }
}
