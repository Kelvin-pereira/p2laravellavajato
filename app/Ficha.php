<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ficha extends Model
{
    protected $fillable = [
        'marca',
        'modelo',
        'placa',
        'preco',
        'users_id',
    ];
}
