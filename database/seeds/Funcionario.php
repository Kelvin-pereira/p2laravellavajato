<?php

use Illuminate\Database\Seeder;

class Funcionario extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('funcionarios')->insert([
            [
                'nome'         => "Maria",
                'cpf'          => "999.999.999-07",
                'rg'           => "333.333.33",
                'telefone'     => "(61) 99999-9999",
                'enderecos_id' => 1,
            ],
            [
                'nome'         => "José",
                'cpf'          => "888.888.888-08",
                'rg'           => "555.555.555",
                'telefone'     => "(61) 88888-8888",
                'enderecos_id' => 1,
            ],
           
        ]);
    }
}
