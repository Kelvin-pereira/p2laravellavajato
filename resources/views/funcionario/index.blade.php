@extends('layouts.padrao')

@section('Conteudo')

<div class="container">
<br>
<div class="row"> <!-- 6 + 6 = 12 -->
    <div class="col-md-5">
        <div class="card" style="width: 20rem;">
            <div class="card-body">
                <h5 class="card-title text-center">Funcionario</h5>
                <form action="/funcionario" method="post" >
                    @csrf   
                    <div class="input-group">
                        <input type="text" name="nome" placeholder="Nome " class="form-control" required>
                    </div><br>
                    <div class="input-group ">
                        <input type="text" name="telefone" placeholder="Telefone" class="form-control" required>
                    </div><br>
                    <div class="input-group">
                        <input type="text" name="cpf" placeholder="CPF" class="form-control" required>
                    </div><br>
                    <div class="input-group">
                        <input type="text" name="rg" placeholder="RG" class="form-control" required>
                    </div><br>
                    <h5 class="card-title text-center">Endereço</h5>
                    <div class="input-group">
                        <input id="cep" type="text" name="cep" placeholder="CEP" class="form-control col-6 mr-2" required>
                        <input id="num" type="text" name="numero" placeholder="num" class="form-control col-6" required>
                    </div><br>
                    <div class="input-group">
                        <input id="localidade" type="text" name="cidade" placeholder="Cidade" class="form-control col-6 mr-2" required>
                        <input id="bairro" type="text" name="bairro" placeholder="Bairro" class="form-control col-6" required>
                    </div><br>
                    <!-- Submit -->
                    <div class="text-right">
                        <button class="btn btn-danger" type="reset">Limpar</button>
                        <button class="btn btn-info" type="submit">Enviar</button>
                    </div>
                {{ Form::close() }}
            </div>
            </div>
    </div>
    {{-- data-table --}}
    <div class="col-md-7">
      
        <table class="table table-hover  table-striped text-center">
                <tr>
                    <th>id</th>
                    <th>Nome</th>
                    <th>CPF</th>
                    <th>RG</th>
                    <th>Açoes</th>
                </tr>
                @foreach ($funcionario as $pessoa)
                    <tr>
                    <td>{{$pessoa->id}}</td>
                    <td>{{$pessoa->nome}}</td>
                    <td>{{$pessoa->cpf}}</td>
                    <td>{{$pessoa->rg}}</td>
                    <td>
                    
                        {{ Form::open([ 'method'  => 'DELETE', 'route' => [ 'funcionario.destroy', $pessoa->id ] ]) }}
                        <a href="funcionario/{{ $pessoa->id }}/edit " class="btn btn-warning ">Editar</a>
                        @csrf   
                        <button class="btn btn-danger" type="submit">Excluir</button>
                        
                        {{ Form::close() }}
            
                    </td>
                    </tr>
                    @endforeach
            </table>
    </div>
</div>
    
</div>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script>
   
$('#cep').change(function(){
    
    $.ajax({
        url: 'http://viacep.com.br/ws/'+$('#cep').val()+'/json',
        
        success: function (dados){
            $('#logradoro').val(dados.localidade);
            $('#bairro').val(dados.bairro);
        }
    });
});

</script>

@endsection