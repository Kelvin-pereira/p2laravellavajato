<?php

namespace App\Http\Controllers\Ficha;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ficha;
use App\FichaAtendimentoFuncionario as FichaAF;

class FichaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fila = Ficha::all();
        return view('home',compact('fila'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ficha = new Ficha($request->all());
        $ficha->users_id = auth()->user()->id;
        $ficha->save();

        $fichaAF = new FichaAF;
        $fichaAF->fichas_id = $ficha->id;
        $fichaAF->status_id = 1;
        $fichaAF->save();
        // $fila = new FichaAF;
        // $fila->

        // $fila = FichaAF::all();

        $fila = Ficha::all();
        return redirect(route('home',compact('fila')) );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
