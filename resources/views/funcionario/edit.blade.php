@extends('layouts.padrao')

@section('Conteudo')
<div class="container">
<br>
<div class="row"> <!-- 6 + 6 = 12 -->
    <div class="col-md-4">
       
    </div>
    <div class="col-md-5">
        <div class="card" style="width: 20rem;">
            <div class="card-body">
                <h5 class="card-title text-center"> Editar Funcionario</h5>
                    {{ Form::open(['method'=>'PUT', 'route'=>['funcionario.update', $func->id ] ]) }}
                    @csrf   
                    <input type="hidden" value=" {{ $func->endereco->id }} " name="enderecoid">
                    <div class="input-group">
                        <input type="text" value="{{$func->nome}}" name="nome" placeholder="Nome " class="form-control" required>
                    </div><br>
                    <div class="input-group ">
                        <input type="text" value="{{$func->telefone}}" name="telefone" placeholder="Telefone" class="form-control" required>
                    </div><br>
                    <div class="input-group">
                        <input type="text" value="{{$func->cpf}}" name="cpf" placeholder="CPF" class="form-control" required>
                    </div><br>
                    <div class="input-group">
                        <input type="text" value="{{$func->rg}}" name="rg" placeholder="RG" class="form-control" required>
                    </div><br>
                    <h5 class="card-title text-center">Endereço</h5>
                    <div class="input-group">
                        <input id="cep" type="text" value="{{ $func->endereco->cep }}" name="cep" placeholder="CEP" class="form-control col-6 mr-2" required>
                        <input id="num" type="text" value="{{ $func->endereco->numero }}" name="numero" placeholder="num" class="form-control col-6" required>
                    </div><br>
                    <div class="input-group">
                        <input id="cidade" type="text" value="{{ $func->endereco->cidade }}" name="cidade" placeholder="Cidade" class="form-control col-6 mr-2" required>
                        <input id="bairro" type="text" value="{{ $func->endereco->bairro }}" name="bairro" placeholder="Bairro" class="form-control col-6" required>
                    </div><br>
                    <!-- Submit -->
                    <div class="text-right">
                        <button class="btn btn-danger" type="reset">Limpar</button>
                        <button class="btn btn-info" type="submit">Enviar</button>
                    </div>
                {{ Form::close() }}
            </div>
            </div>
    </div>
   
</div>
    
</div>

<script>

</script>

@endsection