function HoursMatrix(parent_element_obj, options, init_value)
{
	var _this=this;

	this.inst_id=parent_element_obj.id;

	this.dhsel_is_mouse_down=false;
	this.options=options;
	this.parent_element=parent_element_obj;

	this.get_opt = function (opt_id, def)
	{
		if (!_this.options) return def;

		return _this.options[opt_id]?_this.options[opt_id]:def;
	}

	this.from_hour=_this.get_opt("from_hour",0);
	this.to_hour=_this.get_opt("to_hour",24);
	if (_this.to_hour>35) _this.to_hour=35;
	this.from_hour_orig=this.from_hour;
	this.to_hour_orig=this.to_hour;
	this.days=_this.get_opt("days",["Dom","Seg","Ter","Qua","Qui","Sex","Sab"]);
	this.hours_step=_this.get_opt("hours_step",1); //1=one hour, 0.5=30 minutes etc.
	this.prev_hours_step=this.hours_step;
	this.expand_hours=_this.get_opt("expand_hours_str","Expand Hours");
	this.reduce_hours=_this.get_opt("reduce_hours_str","Reduce Hours");
	this.one_hour_slot=_this.get_opt("one_hour_slot_str","One Hour Slot");
	this.half_hour_slot=_this.get_opt("half_hour_slot_str","Half an Hour Slot");
	this.quarter_hour_slot=_this.get_opt("quarter_hour_slot_str","Quarter of an Hour Slot");
	this.view12=(_this.get_opt("hours_view",24)==12);
	this.hours_vertical=_this.get_opt("hours_vertical",false);
	this.hours_label=_this.get_opt("hours_label",null);

	this.listeners=new Array(); //hold mouse event listeners objects

	this.start_sel=null; //which day+hour was first click on a selection
	this.last_changed_sel=null; //which day+hour was last selected/unselected
	this.select_mode=true; //true=select status false=unselect status

	this.hspacer_ref=null; //used in horizontal mode

	this.set_hours_step=function (step, skip_redraw)
	{
		skip_redraw=skip_redraw||false;
		step=parseFloat(step);
		if (step==NaN) return;
		if ((step!=1)&&(step!=0.5)&&(step!=0.25)) return;

		_this.prev_hours_step=_this.hours_step;
		_this.hours_step=step;
		if (!skip_redraw) _this.draw_matrix();
	}

	this.set_hours_step_1=function () {_this.set_hours_step(1);};
	this.set_hours_step_half=function () {_this.set_hours_step(0.5);};
	this.set_hours_step_quarter=function () {_this.set_hours_step(0.25);};

	this.set_hours_range=function (from_hour, to_hour)
	{
		if (from_hour!=null)
			_this.from_hour=Math.max(from_hour,0);

		if (to_hour!=null)
			_this.to_hour=Math.min(to_hour,35);

		_this.draw_matrix();
	}

	this.set_12_hours_view=function (b)
	{
		_this.view12=b;
		_this.draw_matrix();
	}

	this.toggle_12_hours_view=function ()
	{
		_this.set_12_hours_view(!_this.view12);
	}

	this.set_hours_vertical_view=function (b)
	{
		_this.hours_vertical=b;
		_this.draw_matrix();
	}

	this.set_hours_label=function(label)
	{
		_this.hours_label=label;
		_this.draw_matrix();
	}

	this.toggle_hours_vertical_view=function ()
	{
		_this.set_hours_vertical_view(!_this.hours_vertical);
	}

	this.hours_expended=function()
	{
		return !(_this.from_hour==_this.from_hour_orig);
	}


	this.toggle_hours_range=function (e, skip_redraw)
	{
		skip_redraw=skip_redraw||false;
		if (!_this.hours_expended())
		{
			//expand hours
			_this.from_hour=0;
			_this.to_hour=24;
		}
		else
		{
			_this.from_hour=_this.from_hour_orig;
			_this.to_hour=_this.to_hour_orig;
		}

		if (!skip_redraw) _this.draw_matrix();
	}

	this.value=function()
	{
		var expended=_this.hours_expended();
		var v={hours_expended:(expended?1:0), hours_step:_this.hours_step, selection:new Array()};

		var hours_range_from=expended?0:_this.from_hour;
		var hours_range_to=Math.min((expended?24:_this.to_hour),24);

		var temp_selection=new Array(); //temp var to save selection with additional some temp info
		for (d=0;d<7;d++)
		{
			var selected_hours=new Array();
			var selected_hours_inx=-1;
			var string_inx="";
			for (var h=hours_range_from;h<hours_range_to;h+=_this.hours_step)
			{
				if (_this.check_if_hour_selected(d,h,h+_this.hours_step,null))
				{
					string_inx+="_"+h;
					var just_update=false;
					if (selected_hours_inx>=0)
					{
						if (selected_hours[selected_hours_inx].to_hour==h)
						{
							selected_hours[selected_hours_inx].to_hour=h+_this.hours_step;
							just_update=true;
						}
					}

					if (!just_update)
					{
						selected_hours.push({from_hour:h,to_hour:h+_this.hours_step});
						selected_hours_inx++;
					}
				}
			}
			if (selected_hours_inx>=0)
			{
				var days_arr=null;
				if (temp_selection[string_inx])
					days_arr=temp_selection[string_inx].days;
				else
					days_arr=new Array();

				days_arr.push(d);
				temp_selection[string_inx]={days:days_arr, hours:selected_hours};
			}
		}

		Object.keys(temp_selection).forEach(function (key)
		{
			v.selection.push(temp_selection[key]);
		});

		return v;
	}

	this.draw_matrix = function(init_value)
	{
		var s='';
		var hours_range=_this.to_hour-_this.from_hour;
		if (init_value) if (!init_value.selection) init_value=null;
		var selection_by_days=_this.build_day_selection(init_value);

		_this.listeners=[];
		var shtml="";
		if (_this.hours_vertical) shtml=_this.draw_h(s,hours_range,selection_by_days,init_value); else shtml=_this.draw_r(s,hours_range,selection_by_days,init_value);
		_this.prev_hours_step=_this.hours_step;
		_this.parent_element.innerHTML=shtml;
		_this.bind_listeners();

		if (_this.hours_vertical)
		{
			// document.getElementById(_this.inst_id+'_'+"hspacer").style.height=(document.getElementById(_this.hspacer_ref).getBoundingClientRect().height/2)+"px";
			// document.getElementById(_this.inst_id+'_'+"wspacer").style.width=(document.getElementById(_this.inst_id+'_'+'hspacer').getBoundingClientRect().width)+"px";
		}
	};

	this.draw_r = function (s,hours_range,selection_by_days,init_value)
	{
		var w=_this.parent_element.getBoundingClientRect().width-parseFloat(window.getComputedStyle(_this.parent_element, null).getPropertyValue('padding-left'))-parseFloat(window.getComputedStyle(_this.parent_element, null).getPropertyValue('padding-right'));
		var show_hours_step=hours_range+1-Math.round(Math.min(w/50+1,hours_range));
		var td_width=(96/(hours_range))*_this.hours_step;

		s+='<table class="hours_sel_ttl"><tr><td style="width:'+(4+td_width/2)+'%;"></td>';
		var pcounter=4+td_width/2;
		for (var h=_this.from_hour+_this.hours_step;h<_this.to_hour;h+=_this.hours_step)
		{
			s+='<td style="width:'+td_width+'%;">';
			pcounter+=td_width;
			if ((h%show_hours_step==0)&&(h % 1 === 0))
				s+=_this.hour_str(h);
			else
				s+="&nbsp;";
			s+='</td>';
		}
		s+='<td style="width:'+(100-pcounter)+'%;"></td></tr></table><table class="hours_sel_cont">';

		for (var d=0;d<7;d++)
		{
			s+='<tr>';
				s+='<td style="border:none;width:4%;" class="cdays">'+_this.days[d]+'</td>';
				for (var h=_this.from_hour;h<_this.to_hour;h+=_this.hours_step)
				{
					var is_selected=false;

					if (init_value)
					{
						//is_selected=_this.check_if_hour_selected_in_init(h,h+_this.hours_step,(selection_by_days[d]?selection_by_days[d]:null));
						var day_to_check=d;
						var from_hour_to_check=h;
						var to_hour_to_check=h+_this.hours_step;
						if (h>=24)
						{
							day_to_check=(d+1)%7;
							from_hour_to_check-=24;
							to_hour_to_check-=24;
						}

						is_selected=_this.check_if_hour_selected_in_init(from_hour_to_check,to_hour_to_check,(selection_by_days[day_to_check]?selection_by_days[day_to_check]:null));
					}
					else
						is_selected=_this.check_if_hour_selected(d,h,h+_this.hours_step,_this.prev_hours_step); //maybe it was selected before?

					var cstr=is_selected?'sel':'';
					if (h>=24) cstr+=' c-overlap';
					s+='<td '+(cstr!=''?('class="'+cstr+'"'):'')+' id="'+(_this.inst_id+'_'+d+'_'+h+'_'+(h+_this.hours_step))+'" style="width:'+td_width+'%;" title="'+(h+_this.hours_step<=24?_this.days[d]:(_this.days[(d+1)%7]))+' '+_this.format_hour(h)+' - '+_this.format_hour(h+_this.hours_step)+'">&nbsp;</td>';
					_this.listeners.push({day:d,from_hour:h,to_hour:(h+_this.hours_step)});
				}
			s+='</tr>';
		}

		s+='</table><div class="hours_footer"><span style="margin:0 4% 0 4%;" title="'+(_this.hours_expended()?_this.reduce_hours:_this.expand_hours)+'" id="'+_this.inst_id+'_'+'expand_hours">'+(_this.hours_expended()?'&gt;&lt;':'&lt;&gt;')+'</span> <span id="'+_this.inst_id+'_'+'hours_step_1" title="'+_this.one_hour_slot+'">1</span> <span id="'+_this.inst_id+'_'+'hours_step_12" title="'+_this.half_hour_slot+'">&frac12;</span> <span id="'+_this.inst_id+'_'+'hours_step_14" title="'+_this.quarter_hour_slot+'">&frac14;</span></div>';
		return s;
	}

	this.hours_touch_start = function(e)
	{
		e.preventDefault();
		_this.hours_mouse_down(e);
	}

	this.last_entered_element=null;
	this.hours_touch_move = function(e)
	{
		e.preventDefault();

		if (!_this.dhsel_is_mouse_down)
		{
			_this.last_entered_element=e.target;
			_this.hours_mouse_down(e);
		}
		else
		{
			var temp=document.elementFromPoint(e.touches[0].clientX, e.touches[0].clientY); //that's the last touched element
			if (temp.tagName!='TD') return;
			if (temp==_this.last_entered_element) return;
			_this.last_entered_element=temp;
			_this.hours_mouse_enter({target:_this.last_entered_element});
		}
	}

	//bind listeners to elements
	this.bind_listeners = function ()
	{
		var i=_this.listeners.length;
		while (i--)
		{
			var eid=_this.inst_id+'_'+_this.listeners[i].day+'_'+_this.listeners[i].from_hour+'_'+_this.listeners[i].to_hour;
			document.getElementById(eid).ontouchstart=_this.hours_touch_move;
			document.getElementById(eid).ontouchmove=_this.hours_touch_move;
			document.getElementById(eid).onmouseenter=_this.hours_mouse_enter;
			document.getElementById(eid).onmousedown=_this.hours_mouse_down;

		}

		document.getElementById(_this.inst_id+"_"+"expand_hours").onclick=_this.toggle_hours_range;
		document.getElementById(_this.inst_id+"_"+"hours_step_1").onclick=_this.set_hours_step_1;
		document.getElementById(_this.inst_id+"_"+"hours_step_12").onclick=_this.set_hours_step_half;
		document.getElementById(_this.inst_id+"_"+"hours_step_14").onclick=_this.set_hours_step_quarter;
	}

	this.draw_h = function (s,hours_range,selection_by_days,init_value)
	{
		s+='<table class="hours_sel_ttl"><tr><td class="calendar-hour-collumn"></td><td><table style="width:100%;"><tr>';
		for (var d=0;d<7;d++)
		{
			s+='<td style="width:13%;">';
			s+=_this.days[d];
			s+='</td>';
		}
		s+='</tr></table></td></tr></table>';

		s+='<table><tr class="calendar-container"><td class="calendar-hour-collumn"><table class="hours_sel_cont" style="width:auto;"><tr id="'+_this.inst_id+'_'+'hspacer"></tr>';
		for (var h=_this.from_hour+_this.hours_step;h<_this.to_hour;h+=_this.hours_step)
		{
			s+='<tr><td class="hour-list">'+_this.format_hour(h,false)+'</td></tr>';
		}

		s+='</table></td><td class="calendar-hour-container"><table class="hours_sel_cont">';

		_this.hspacer_ref='';
		for (var h=_this.from_hour;h<_this.to_hour;h+=_this.hours_step)
		{
			s+='<tr>';
				for (var d=0;d<7;d++)
				{
					var is_selected=false;

					if (init_value)
					{
						//is_selected=_this.check_if_hour_selected_in_init(h,h+_this.hours_step,(selection_by_days[d]?selection_by_days[d]:null));
						var day_to_check=d;
						var from_hour_to_check=h;
						var to_hour_to_check=h+_this.hours_step;
						if (h>=24)
						{
							day_to_check=(d+1)%7;
							from_hour_to_check-=24;
							to_hour_to_check-=24;
						}

						is_selected=_this.check_if_hour_selected_in_init(from_hour_to_check,to_hour_to_check,(selection_by_days[day_to_check]?selection_by_days[day_to_check]:null));
					}
					else
						is_selected=_this.check_if_hour_selected(d,h,h+_this.hours_step,_this.prev_hours_step); //maybe it was selected before?

					var cstr=is_selected?'sel':'';
					if (h>=24) cstr+=' c-overlap';
					s+='<td '+(cstr!=''?('class="'+cstr+'"'):'')+' id="'+(_this.inst_id+'_'+d+'_'+h+'_'+(h+_this.hours_step))+'" style="width:13%;" title="'+(h+_this.hours_step<=24?_this.days[d]:(_this.days[(d+1)%7]))+' '+_this.format_hour(h)+' - '+_this.format_hour(h+_this.hours_step)+'">&nbsp;</td>';
					_this.listeners.push({day:d,from_hour:h,to_hour:(h+_this.hours_step)});
					if (_this.hspacer_ref.length<1) _this.hspacer_ref=_this.inst_id+'_'+d+'_'+h+'_'+(h+_this.hours_step);
				}

			s+='</tr>';
		}
		s+='</table></td></tr></table><div class="hours_footer"><span style="margin:0 4% 0 4%;" title="'+(_this.hours_expended()?_this.reduce_hours:_this.expand_hours)+'" id="'+_this.inst_id+'_'+'expand_hours">'+(_this.hours_expended()?'&gt;&lt;':'&lt;&gt;')+'</span> <span id="'+_this.inst_id+'_'+'hours_step_1" title="'+_this.one_hour_slot+'">1</span> <span id="'+_this.inst_id+'_'+'hours_step_12" title="'+_this.half_hour_slot+'">&frac12;</span> <span id="'+_this.inst_id+'_'+'hours_step_14" title="'+_this.quarter_hour_slot+'">&frac14;</span></div>';
		return s;

	}


	this.check_if_hour_selected_in_init = function(from_hour,to_hour,day_selection)
	{
		if (!day_selection) return false;

		for (var key=day_selection.length-1;key>=0;key--)
		{
			if (parseFloat(day_selection[key].from_hour)<=parseFloat(from_hour))
				if (parseFloat(day_selection[key].to_hour)>=parseFloat(to_hour))
					return true;
		}

		return false;
	}

	//fetch the selection for each day from init selection
	this.build_day_selection = function(init_value)
	{
		if (!init_value) return;
		var day_selection=new Array();
		Object.keys(init_value.selection).forEach(function (key)
		{
			var s=init_value.selection[key];
			if (s.days)
			{
				Object.keys(s.days).forEach(function (key2)
				{
					day_selection[s.days[key2]]=s.hours;
				});
			}
		});

		return day_selection;
	}

	//see if hour is selected
	this.check_if_hour_selected = function (day,from_hour,to_hour,prev_hours_step)
	{
		if (prev_hours_step!=null)
		{
			//calc by different hours step (probably before resize)
			if (prev_hours_step!=_this.hours_step)
			{
				//need to adjust ranges
				from_hour=Math.floor(from_hour/prev_hours_step)*prev_hours_step;
				to_hour=Math.ceil(to_hour/prev_hours_step)*prev_hours_step;
			}

			if (prev_hours_step<_this.hours_step)
			{
				//we have a range to check
				for (var h=from_hour;h<to_hour;h+=prev_hours_step)
				{
					var e=document.getElementById(_this.inst_id+'_'+day+'_'+h+'_'+(h+prev_hours_step));
						if (!e) return false;
					if (!e.classList.contains("sel")) return false;
				}
				return true;
			}
		}

		var e=document.getElementById(_this.inst_id+'_'+day+'_'+from_hour+'_'+to_hour);
		if (!e) return false;
		return e.classList.contains("sel");
	}

	//that's hours only - without minutes
	this.hour_str = function (h, skip_am_pm)
	{
		skip_am_pm=skip_am_pm||false;

		var s="";

		if ((_this.view12)&&(h!=0))
		{
			s=h==0?"12":(h>12?(h-12).toString():h.toString());
			if (!skip_am_pm)
				s+=h<12?"am":"pm";
		}
		else
			s=h.toString();

		return s;
	}

	//full hours - including minutes
	this.format_hour = function (h,skip_minutes)
	{
		if (h>=24) h-=24;
		var m=(h%1)*60;
		h=Math.floor(h);
		hstr=_this.hour_str(h, true);
		if (hstr.length<2) hstr="0"+hstr;
		if (_this.hours_label)
		{
			if (_this.hours_label[h])
			{
				hstr=_this.hours_label[h];
				skip_minutes=true;
			}
		}
		return hstr+(skip_minutes?"":(":"+(m<10?("0"+m):m)+(_this.view12?(h<12?"am":"pm"):"")));
	}

	this.hours_mouse_down = function (e)
	{
		var temp=_this.get_element_parts(e.target.id);
		var day=temp.day;
		var from_hour=temp.from_hour;
		var to_hour=temp.to_hour;

		_this.dhsel_is_mouse_down=true;
		_this.select_mode=_this.set_select(day,from_hour,to_hour,null);
		_this.last_changed_sel=null;
		_this.start_sel={day:parseInt(day),from_hour:parseFloat(from_hour),to_hour:parseFloat(to_hour)};
		e.preventDefault();
	}

	//get element id and returns parts (day, from_hour, to_hour)
	this.get_element_parts = function (eid)
	{
		var p=eid.replace(_this.inst_id,'').split("_");
		return {day:p[1],from_hour:p[2],to_hour:p[3]};
	}

	this.hours_mouse_enter = function (e) //,day,from_hour,to_hour)
	{
		if (!_this.dhsel_is_mouse_down) return;

		var temp=_this.get_element_parts(e.target.id);
		var day=parseInt(temp.day);
		var from_hour=parseFloat(temp.from_hour);
		var to_hour=parseFloat(temp.to_hour);

		_this.set_select(day,from_hour,to_hour,_this.select_mode);

		//console.log({day:day,from_hour:from_hour,to_hour:to_hour});

		//maybe some more to select/unselect?
		if (_this.last_changed_sel)
		{
			if (day!=_this.last_changed_sel.day)
			{
				var day_direction=day>_this.last_changed_sel.day?1:-1;

				for (var h=Math.min(_this.start_sel.from_hour,from_hour);h<=Math.max(_this.start_sel.from_hour,from_hour);h+=_this.hours_step)
					_this.set_select(day,h,h+_this.hours_step,_this.select_mode);

				if (((day>=_this.start_sel.day)&&(day_direction<0)) || ((day<=_this.start_sel.day)&&(day_direction>0)) )
				{
					for (var h=Math.min(_this.start_sel.from_hour,from_hour);h<=Math.max(_this.start_sel.from_hour,from_hour);h+=_this.hours_step)
						_this.set_select(_this.last_changed_sel.day,h,h+_this.hours_step,!(_this.select_mode));
				}
			}

			if (from_hour!=_this.last_changed_sel.from_hour)
			{
				var hour_direction=from_hour>_this.last_changed_sel.from_hour?1:-1;

				for (var d=Math.min(_this.start_sel.day,day);d<=Math.max(_this.start_sel.day,day);d++)
					_this.set_select(d,from_hour,to_hour,_this.select_mode);

				if (((from_hour>=_this.start_sel.from_hour)&&(hour_direction<0)) || ((from_hour<=_this.start_sel.from_hour)&&(hour_direction>0)) )
				{
					for (var d=Math.min(_this.start_sel.day,day);d<=Math.max(_this.start_sel.day,day);d++)
						_this.set_select(d,_this.last_changed_sel.from_hour,_this.last_changed_sel.to_hour,!(_this.select_mode));
				}
			}

		}


		_this.last_changed_sel={day:day,from_hour:from_hour,to_hour:to_hour};
	}

	//select_mode:
	//true=select, false=unselect, null=reverse
	//returns select_mode
	this.set_select = function (day,from_hour,to_hour,select_mode,no_overlapping)
	{
		//console.log("day: "+day+" fh: "+from_hour+" th: "+to_hour);

		var e=document.getElementById(_this.inst_id+'_'+day+'_'+from_hour+'_'+to_hour);
		if (!e) return _this.select_mode;

		if (select_mode==null)
			select_mode=!(e.classList.contains("sel"));

		if (select_mode)
			e.classList.add("sel");
		else
			e.classList.remove("sel");

		no_overlapping=no_overlapping||false;
		if (!no_overlapping)
		{
			//overlapping hours?
			if (to_hour>24)
				//next day
				_this.set_select((parseInt(day)+1)%7, parseFloat(from_hour)-24, parseFloat(to_hour)-24, select_mode,true);
			else if (_this.to_hour>24)
				//prev day (notive the complicated mod, that's javascript bug
				_this.set_select((((parseInt(day)-1)%7)+7)%7, parseFloat(from_hour)+24, parseFloat(to_hour)+24, select_mode,true);
		}

		return select_mode;
	}

	//if there is an init value, get parameters from it (and overrides if needed)
	if (init_value)
	{
		if (init_value.hours_expended)
			if (init_value.hours_expended=="1")
				if (!this.hours_expended)
					this.toggle_hours_range(null, true);

		if (init_value.hours_step)
			this.set_hours_step(init_value.hours_step, true);
	}

	this.draw_matrix(init_value); //at constructor

	this.set_mouse_is_up = function ()
	{
		_this.dhsel_is_mouse_down=false;
		_this.last_entered_element=null;
	}

	document.addEventListener("mouseup", _this.set_mouse_is_up);
	document.addEventListener("touchend", _this.set_mouse_is_up);
}
