@extends('layouts.login')

@section('content')

    <section class="login">
        <div class="login__form">
            <div class="login__form-icon">
            
            </div>

         


         

            {{ Form::open(['url'=>'/register','method'=>'POST','id'=>'form','class'=>'form' ] ) }}

            {{ csrf_field() }}

            {{ Form::text('name',null,['class' => 'form-control','placeholder'=>'Nome','required']) }}

            {{ Form::email('email',null,['class' => 'form-control','placeholder'=>'Email','required']) }}

            {{Form::password('password',['class'=>'form-control','placeholder'=>'Senha','required'])}}

            {{Form::password('password_confirm',['class'=>'form-control','placeholder'=>'Confirmar senha','required'])}}

            {{Form::submit('Cadastrar')}}

      

            {{Form::close()}}

            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
            @endif

            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
            @endif


        </div>
    </section>

@endsection