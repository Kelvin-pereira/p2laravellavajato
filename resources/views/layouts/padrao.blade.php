<!DOCTYPE html>
<html class="no-js" lang="pt-br">
<head>
    <meta charset="utf-8">
    <title>Nome Criativo</title>
    <link href="{{ asset('assets/stylesheets/application.bundle-6f1ad028.css') }}" rel="stylesheet" media="all"/>
    <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
    <link href="//cdnjs.cloudflare.com" rel="dns-prefetch">
    <link href="//www.google-analytics.com" rel="dns-prefetch">
    <link href="{{ asset('assets/images/favicon-57e24d0d.png') }}" rel="shortcut icon">

    <script src="js/jquery-1.8.2.min.js" type="text/javascript" ></script>
    {{-- bootstrap --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    {{-- end --}}
    <meta content="initial-scale=1, user-scalable=yes, width=device-width" name="viewport">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="on" http-equiv="cleartype">
    <meta content="True" name="HandheldFriendly">
    <meta content="320" name="MobileOptimized">
    <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script><![endif]-->
    <script src="{{ asset('assets/javascripts/hours_matrix-568a9f05.js') }} "></script>


</head>
<body>
<noscript>
    <div class="browser-warning">
        <p>
            Your browser has <strong>javascript turned off</strong>. Please <a
                    href="http://www.activatejavascript.org/">enable javascript</a>, or you may need to <a
                    href="http://browsehappy.com/">upgrade your browser</a>.
        </p>
    </div>
</noscript>
<section class="navbar">
    <div class="navbar__container">
            <div class="navbar__user__welcome-msg">
                    BEM VINDO(a)<br><b> {{auth()->user()->name}} </b>
                </div>
                <a href="/funcionario">Cadastrar funcionario</a>
                <a href="/home">Home</a>
                <a href="/fila">Fila</a>
        <a href="/home" class="navbar__brand">


        </a>
        <div class="navbar__user">
            

            <div class="navbar__user__cta">

                {{ Form::open(['url'=>'/logout', 'method' =>'post']) }}

                {{ Form::submit('Sair',['class'=>'btn btn--default']) }}

                {{Form::close()}}

            </div>

        </div>
    </div>
</section>

@yield('Conteudo')

{{-- bootstrap --}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<script src="{{ asset('assets/javascripts/application.bundle-f11a826d.js') }}" defer="defer"></script>
{{-- end bootstrap --}}
<!--[if lt IE 9]>
<script src="//cdnjs.cloudflare.com/ajax/libs/nwmatcher/1.3.6/nwmatcher.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script><![endif]-->
</body>
</html>